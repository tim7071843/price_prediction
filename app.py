from flask import Flask, render_template, request
from predict import PricePredict

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file:
            if file.filename.endswith('.csv'):
                pt = PricePredict(file)
                predict_price = pt.predict()
            return render_template('result.html', predict_price=predict_price)
    
    return render_template('upload.html')

if __name__ == '__main__':
    app.run()
