import pandas as pd
import numpy as np
from datetime import date
from sklearn.preprocessing import MinMaxScaler #歸一化
from tensorflow import keras

class PricePredict():
    def __init__(self, data):
        self.historical_data = pd.read_csv(data, header=1)  #從第二列開始提取
    
    def predict(self):
        # 指定第一行作为列名
        if len(self.historical_data) == 100:
            self.historical_data = self.historical_data.drop(columns=["成交金額","除權值","Unnamed: 8"]) #刪除不必要欄位
            self.historical_data['日期'] = pd.to_datetime(self.historical_data['日期'], format='%Y%m%d') #修改日期格式
            # today_date = date.today()
            # last_index = self.historical_data.index[-1]
            # if last_index.date() == today_date:
            #     self.historical_data = self.historical_data[:-1] #載入資料最新日期還正在開盤，可能不準確，所以刪除
            self.historical_data.set_index("日期", inplace=True)
            #刪除過於久遠的資料
            self.historical_data = self.historical_data[-100:] #這行在正式預測可以不要
            
            closing_prices = pd.DataFrame(self.historical_data["收盤價"],columns=["收盤價"])
            
            x_scaler=MinMaxScaler()
            y_scaler=MinMaxScaler()
            scaled_input_data=pd.DataFrame(x_scaler.fit_transform(self.historical_data),columns=self.historical_data.columns)
            scaled_target_data=pd.DataFrame(y_scaler.fit_transform(closing_prices),columns=["收盤價"])
            
            MOVING = 100  # 提取100天数据
            input_window_data = []
            
            for i in range(len(scaled_input_data) - MOVING + 1):
                x = scaled_input_data[i:i+MOVING]  # 一百天的開高低收
                input_window_data.append(x)
            
            input_window_data = np.array(input_window_data) #型態轉換
            
            model = keras.models.load_model("model.hdf5")
            preds = model.predict(input_window_data)
            preds = y_scaler.inverse_transform(preds)
            return preds
        else:
            return "請輸入正確格式"

a = PricePredict("0050_test.csv")
b = a.predict()
print(b)
